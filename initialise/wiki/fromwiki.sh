#!/bin/bash

baseurl='http://conwaylife.com/w/index.php?title=Category:Patterns&'
urlsuffix='pagefrom=0'

while [ ! -z "$urlsuffix" ]; do

    curl "$baseurl$urlsuffix" > "download.txt"
    cat "download.txt" | grep -o '<li><a href="/wiki/[^"]*"' | grep -v ':' | grep -o '"[^"]*"'
    urlsuffix="$( cat "download.txt" | grep -o 'pagefrom=[^"]*#' | grep -o '[^#]*' | head -n 1 )"

done
