#!/bin/bash

set -e

mvn -B install
exec 3< <( mvn -B appengine:devserver 2>&1 )

while read line; do

    echo "$line"
    if echo "$line" | grep 'is running at'; then
        break
    fi

done <&3

sleep 1

echo "Initialising Catagolue..."

cd initialise
python process.py "test_password" "http://localhost:8080"

while read line; do

    echo "$line"

done <&3
