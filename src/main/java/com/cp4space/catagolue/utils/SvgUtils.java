package com.cp4space.catagolue.utils;

import java.util.regex.Pattern;
import java.io.PrintWriter;
import com.cp4space.catagolue.algorithms.SmallLife;

public class SvgUtils {

    public static void canToSvg(PrintWriter writer, String rulestring, String canonised, int period, int maxwidth, int maxheight, int maxcell) {
        int ep = ((rulestring.charAt(0) == 'b') && (!(rulestring.contains("f") || rulestring.contains("h")))) ? period : 0;
        SmallLife pattern = new SmallLife(canonised.getBytes(), ep);
        pattern.propagate(rulestring);
        pattern.getSvg(writer, maxwidth, maxheight, maxcell);
    }

    public static void apgToSvg(PrintWriter writer, String rulestring, String apgcode, int maxwidth, int maxheight, int maxcell) {
        if ((apgcode != null) && apgcode.contains("_") && Pattern.matches("[a-y][a-y][0-9]*(_[a-z0-9]+)+", apgcode)) {
            // http://ferkeltongs.livejournal.com/15837.html
            String[] parts = apgcode.split("_", 2);
            String part1 = parts[0];
            String part2 = parts[1];
            if ((part1.length() >= 3) && (part1.charAt(0) == 'x')) {
                int period = Integer.valueOf(part1.substring(2));
                if ((part1.charAt(1) == 's') || (rulestring.charAt(1) == '0') || (period > 100)) { period = 0; }
                canToSvg(writer, rulestring, part2, period, maxwidth, maxheight, maxcell);
            } else if (apgcode.equals("yl144_1_16_afb5f3db909e60548f086e22ee3353ac")) {
                if (maxwidth <= 160) {
                    writer.println("<img src=\"/images/BLSE-160px.gif\" width=\""+maxwidth+"\" />");
                } else {
                    writer.println("<img src=\"/images/BLSE-320px.gif\" width=\""+maxwidth+"\" />");
                }
            } else if (apgcode.equals("yl384_1_59_7aeb1999980c43b4945fb7fcdb023326")) {
                if (maxwidth <= 160) {
                    writer.println("<img src=\"/images/GPSE-160px.gif\" width=\""+maxwidth+"\" />");
                } else {
                    writer.println("<img src=\"/images/GPSE-320px.gif\" width=\""+maxwidth+"\" />");
                }
            } else if (apgcode.equals("yl12_1_8_c7310da81295b6611e6e4e34a80a5523")) {
                if (maxwidth <= 160) {
                    writer.println("<img src=\"/images/PFSH-160px.gif\" width=\""+maxwidth+"\" />");
                } else {
                    writer.println("<img src=\"/images/PFSH-320px.gif\" width=\""+maxwidth+"\" />");
                }
            } else {
                questionMarks(writer);
            }
        } else {
            questionMarks(writer);
        }
    }

    public static void questionMarks(PrintWriter writer) {
        writer.println("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 120 100\" width=\"120px\" height=\"100px\">");
        writer.println("<text x=\"0\" y=\"80\" font-family=\"Helvetica\" font-size=\"100\" fill=\"gray\">??</text></svg>");
    }

    public static void apgRow(PrintWriter writer, String rulestring, String objname, String... strings) {

        apgRow(-1, writer, rulestring, objname, strings);

    }

    public static void apgRow(int i, PrintWriter writer, String rulestring, String objname, String... strings) {

        apgRow(true, i, writer, rulestring, objname, strings);

    }

    public static void apgRow(boolean includeImages, int i, PrintWriter writer, String rulestring, String objname, String... strings) {

        String[] parts = objname.split(" ",2);
        String apgcode = parts[0];
        String commonname = (parts.length >= 2 ? parts[1] : null);

        writer.println("<tr>");
        if (i >= 0) {
            writer.println("<td>" + i + "</td>");
        }

        if (includeImages) {
            writer.println("<td align=\"center\">");

            apgToSvg(writer,rulestring,apgcode,160,160,8);
            // writer.println("<img src=\"/pic/" + apgcode + "/" + rulestring + "?maxsize=160&maxcell=8\" />");

            writer.println("</td>");
        }
        writer.println("<td><a href=\"/object/" + apgcode + "/" + rulestring + "\">");
        if (apgcode.length() <= 48) {
            writer.println(apgcode + "</a>");
        } else {
            writer.println(apgcode.substring(0, 22) + "..." + apgcode.substring(apgcode.length() - 22) + "</a>");
        }
        if (commonname != null) {
            writer.println(commonname);
        }
        writer.println("</td>");
        for (String item : strings) {
            writer.println("<td>" + item + "</td>");
        }
        writer.println("</tr>");
    }

}
