package com.cp4space.catagolue.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pegdown.Printer;
import org.pegdown.VerbatimSerializer;
import org.pegdown.ast.VerbatimNode;

public class RleVerbatimSerializer implements VerbatimSerializer {

    static final Pattern getDimensions = Pattern.compile("x=([0-9]+),y=([0-9]+).*");

    static final String[] colours = {"black", "red", "orange", "yellow", "green", "blue", "indigo", "violet"};

    public void drawCell(int x, int y, int value, Printer printer) {

        if (value > 0) {
            String opacity = ((value % 2) == 1 ? "1.0" : "0.3");
            String colour = colours[(value - 1)/2 % 8];

            printer.println().print("<rect width=\"7\" height=\"7\" x=\"" + 8*x + "\" y=\"" + 8*y +
                    "\" rx=\"2\" ry=\"2\" fill=\"" + colour + "\" opacity=\"" + opacity + "\"></rect>");
        }

    }

    public void writeHeader(int width, int height, Printer printer) {
        double gsize = Math.min(6.0, Math.min(288.0 / width, 640.0 / height));
        printer.println().print("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 " +
                8*width + " " + 8*height + "\" width=\"" + gsize*width + "px\" height=\"" + gsize*height + "px\">");
    }

    public boolean rle2svg(String[] lines, final Printer printer) {

        int maxwidth = 144;
        int maxheight = 320;

        int width = maxwidth;
        int height = maxheight;

        int x = 0;
        int y = 0;
        int count = 0;
        int colour = 0;

        boolean headerRequired = true;

        for (String line : lines) {
            String trimmed = line.replaceAll("\\s","");
            if (trimmed.length() >= 2 && trimmed.charAt(0) == 'x' && trimmed.charAt(1) == '=') {
                // header line
                Matcher dimensions = getDimensions.matcher(trimmed);
                if (dimensions.find()) {
                    width = Integer.valueOf(dimensions.group(1));
                    height = Integer.valueOf(dimensions.group(2));
                }
            } else if (trimmed.length() >= 1 && trimmed.charAt(0) == '#') {
                // comment line

            } else if (trimmed.length() >= 1) {
                // code line
                // write the SVG header if necessary:
                if (headerRequired) {
                    if ((width <= maxwidth) && (height <= maxheight)) {
                        headerRequired = false;
                        writeHeader(width, height, printer);
                    }
                }

                if (headerRequired == false) {
                    char[] characters = trimmed.toCharArray();

                    for (char c : characters) {
                        if ((c >= '0') && (c <= '9')) {
                            count *= 10;
                            count += (c - '0');
                        } else if ((c == 'b') || (c == '.')) {
                            if (count == 0) { count = 1; }
                            x += count; count = 0;
                        } else if (c == '$') {
                            if (count == 0) { count = 1; }
                            y += count; x = 0; count = 0;
                            if (y >= height) { return headerRequired; }
                        } else if ((c == 'o') || ((c >= 'A') && (c <= 'X'))) {
                            if (count == 0) { count = 1; }
                            if (c == 'o') {
                                colour = colour * 24 + 1;
                            } else {
                                colour = colour * 24 + (c - 'A') + 1;
                            }
                            for (int i = 0; i < Math.min(count, width - x); i++) {
                                drawCell(x + i, y, colour, printer);
                            }
                            x += count; count = 0; colour = 0;
                        } else if ((c >= 'p') && (c <= 'z')) {
                            int m = c - 'o';
                            colour = colour * 11 + (m % 11);
                        } else if (c == '!') { return headerRequired; }
                    }
                }
            }
        }

        return headerRequired;

    }

    @Override
    public void serialize(final VerbatimNode node, final Printer printer) {

        String text = node.getText();

        String[] lines = text.split("\n");

        printer.println().print("<table><tr><td>");

        boolean headerRequired = rle2svg(lines, printer);

        if (headerRequired == false) {
            printer.println().print("</svg>");
        }

        printer.println().print("</td><td><pre><code>");

        for (String line : lines) {
            String trimmed = line.replaceAll("\\s","");
            if (trimmed.length() >= 2 && trimmed.charAt(0) == 'x' && trimmed.charAt(1) == '=') {
                // header line
                printer.print("<b><font color=\"#7f007f\">").printEncoded(line.trim()).print("</font></b>").println();
            } else if (trimmed.length() >= 1 && trimmed.charAt(0) == '#') {
                // comment line
                printer.print("<font color=\"#007f00\">").printEncoded(line.trim()).print("</font>").println();
            } else if (trimmed.length() >= 1) {
                // code line
                printer.printEncoded(line.trim()).println();
            }
        }

        printer.print("</code></pre></td></tr></table>");
    }

}
