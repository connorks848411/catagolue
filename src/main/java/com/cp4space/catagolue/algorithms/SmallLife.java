package com.cp4space.catagolue.algorithms;

import java.io.PrintWriter;
import java.util.ArrayList;

public class SmallLife {
    
    // Container for a universe with fixed width, height and time.
    private final byte[][][] universe;
    private final int width;
    private final int height;
    public final int lifespan;
    
    public SmallLife(byte[] canonised, int period) {
        
        int maxx = 0;
        int maxy = 0;
        int x = 0;
        int y = 0;
        int z = 0;
        boolean iny = false;
        lifespan = (period < 0) ? 0 : period;
        
        int l = canonised.length;
        
        // Convert canonised representation into cell list:
        
        ArrayList<Integer> celllist = new ArrayList<Integer>();
        
        for (int i = 0; i < l; i++) {
            int c = -1;
            
            if ((canonised[i] >= 97) && (canonised[i] < 123)) {
                c = canonised[i] - 87;
            } else if ((canonised[i] >= 48) && (canonised[i] < 58)) {
                c = canonised[i] - 48;
            } else if ((canonised[i] >= 65) && (canonised[i] < 91)) {
                c = canonised[i] - 55;
            } else if (canonised[i] == 95) {
                z += 1; x = 0; y = 0;
            }
            
            if (c >= 0) {
                if (iny) {
                    iny = false; x += c;
                } else if (c < 32) {
                    for (int j = 0; j < 5; j++) {
                        if ((c & (1 << j)) > 0) {
                            celllist.add(x);
                            celllist.add(y + j);
                            celllist.add(z);
                            maxx = Math.max(x, maxx);
                            maxy = Math.max(y + j, maxy);
                        }
                    }
                    x += 1;
                } else if (c == 35) {
                    x = 0; y += 5;
                } else {
                    x += (c - 30);
                    iny = (c == 34);
                }
            }
        }
        
        // Create empty universe:
        
        width = maxx + 2 * lifespan + 3;
        height = maxy + 2 * lifespan + 3;
        universe = new byte[lifespan + 1][height][width];
        
        // Populate first layer:
        
        for (int i = 0; i < celllist.size(); i += 3) {
            universe[0][celllist.get(i + 1) + lifespan + 1][celllist.get(i) + lifespan + 1] |= (1 << celllist.get(i + 2));
        }
        
    }
    
    public void propagate(String rulestring) {
        
        // boolean[] bee = new boolean[9];
        // boolean[] ess = new boolean[9];
        
        String[] bee = new String[10];
        String[] ess = new String[10];

        String lord = "";

        lord += "_ceaccaieaeaknja_ceaccaieaeaknjaekejanaairerririekejanaairerriri";
        lord += "ccknncqnaijaqnwaccknncqnaijaqnwakykkqyqjrtjnzrqakykkqyqjrtjnzrqa";
        lord += "ekirkyrtejerkkjnekirkyrtejerkkjnekejjkrnejecjyccekejjkrnejecjycc";
        lord += "anriqyzraariqjqaanriqyzraariqjqajkjywkqkrnccqkncjkjywkqkrnccqknc";
        lord += "cnkqccnnkqkqyykjcnkqccnnkqkqyykjaqjwinaarzjqtrnaaqjwinaarzjqtrna";
        lord += "ccyyccyennkjyekeccyyccyennkjyekenykknejeirykrikenykknejeirykrike";
        lord += "aqrznyirjwjqkkykaqrznyirjwjqkkykaqrqajiarqcnnkccaqrqajiarqcnnkcc";
        lord += "intrneriaanajekeintrneriaanajekeajnkaeaeiaccaec_ajnkaeaeiaccaec_";

        int lastloc = 9;

        boolean birth = false;
        
        byte[] rulebytes = rulestring.getBytes();
        
        int l = rulebytes.length;
        
        for (int i = 0; i < l; i++) {
            byte c = rulebytes[i];
            
            if ((c >= 48) && (c <= 56)) {
                lastloc = c - 48;
                if (birth) {
                    bee[lastloc] = "+";
                } else {
                    ess[lastloc] = "+";
                }
            } else if ((c == 98) || (c == 66)) {
                birth = true;
            } else if ((c == 115) || (c == 83)) {
                birth = false;
            } else if (c == 47) {
                birth = ! birth;
            } else if ((c == 45) || ((c >= 97) && (c <= 122))) {
                if (c == 118) {
                    c = 110;
                }
                if (birth) {
                    bee[lastloc] += ((char) c);
                } else {
                    ess[lastloc] += ((char) c);
                }
            }
        }

        byte[] lordbytes = lord.getBytes();
        int[] lord2 = new int[512];
        int[] popcounts = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};

        for (int i = 0; i < 512; i++) {
            int centre = (i & 16) >> 4;
            int ncount = popcounts[i & 15] + popcounts[i >> 5];
            String fragment = (centre > 0) ? ess[ncount] : bee[ncount];

            if (fragment == null || fragment.length() == 0) {
                lord2[i] = 0;
            } else if (fragment.length() == 1) {
                lord2[i] = 1;
            } else {
                byte[] fragbytes = fragment.getBytes();
                lord2[i] = (fragbytes[1] == 45) ? 1 : 0;
                for (byte c : fragbytes) {
                    if (c == lordbytes[i]) {
                        lord2[i] = 1 - lord2[i];
                    }
                }
            }
        }
        
        for (int i = 0; i < lifespan; i++) {
            for (int y = 1; y < height - 1; y++) {
                for (int x = 1; x < width - 1; x++) {
                    int count = 0;
                    if (universe[i][y-1][x-1] == 1) { count += 1; }
                    if (universe[i][y  ][x-1] == 1) { count += 2; }
                    if (universe[i][y+1][x-1] == 1) { count += 4; }
                    if (universe[i][y-1][x  ] == 1) { count += 8; }
                    if (universe[i][y  ][x  ] == 1) { count += 16; }
                    if (universe[i][y+1][x  ] == 1) { count += 32; }
                    if (universe[i][y-1][x+1] == 1) { count += 64; }
                    if (universe[i][y  ][x+1] == 1) { count += 128; }
                    if (universe[i][y+1][x+1] == 1) { count += 256; }
                    universe[i+1][y][x] = (lord2[count] > 0) ? ((byte) 1) : ((universe[i][y][x] > 0) ? ((byte) 2) : ((byte) 0));
                }
            }
        }
    }
    
    public void getAscii(PrintWriter writer, int layer) {
        writer.println("<pre>");
        for (int y = 0; y < height; y++) {
            String s = "";
            for (int x = 0; x < width; x++) {
                if (universe[layer][y][x] == 1) {
                    s += "*";
                } else {
                    s += ".";
                }
            }
            writer.println(s);
        }
        writer.println("</pre>");
    }
    
    public boolean checkEquality(int offx, int offy, int period) {
        
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (universe[0][y][x] == 1) {
                    if (x + offx >= width) {
                        return false;
                    } else if (x + offx < 0) {
                        return false;
                    } else if (y + offy >= height) {
                        return false;
                    } else if (y + offy < 0) {
                        return false;
                    } else if (universe[period][y + offy][x + offx] != 1) {
                        return false;
                    }
                }
                
                if (universe[period][y][x] == 1) {
                    if (x - offx >= width) {
                        return false;
                    } else if (x - offx < 0) {
                        return false;
                    } else if (y - offy >= height) {
                        return false;
                    } else if (y - offy < 0) {
                        return false;
                    } else if (universe[0][y - offy][x - offx] != 1) {
                        return false;
                    }
                }
            }
        }
        
        return true;
        
    }
    
    public boolean validatePeriod() {
        
        if (lifespan > 0) {
            
            int[] offset = getOffset();
            int offx = offset[0];
            int offy = offset[1];
            
            if (!checkEquality(offx, offy, lifespan)) {
                // The pattern differs between generations 0 and lifespan:
                return false;
            } else {
                for (int q = 2; q <= lifespan; q++) {
                    // Check that q divides the lifespan and offsets:
                    if ((lifespan % q == 0) && (offx % q == 0) && (offy % q == 0)) {
                        if (checkEquality(offx/q, offy/q, lifespan/q)) {
                            // The pattern has a proper divisor of the purported period:
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
    
    public int getPopulation(int generation) {
        int pop = 0;
        if (generation >= 0 && generation <= lifespan) {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (universe[generation][y][x] == 1) {
                        pop += 1;
                    }
                }
            }
        }
        return pop;
    }
    
    public int[] getOffset() {
        
        // Returns a pair (horizontal offset, vertical offset).
        
        int[] offset = new int[2];
        int offx = 0;
        int offy = 0;
        int offz = 0;
        int offw = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // Determine the centroid:
                if (universe[lifespan][y][x] == 1) {
                    offx += x;
                    offy += y;
                    offz += 1;
                }
                if (universe[0][y][x] == 1) {
                    offx -= x;
                    offy -= y;
                    offw += 1;
                }
            }
        }
        
        if ((offz == offw) && (offz != 0)) {
            // Mass conservation:
            offx = offx / offz;
            offy = offy / offz;
        } else {
            offx = 0;
            offy = 0;
        }
        
        offset[0] = offx;
        offset[1] = offy;
        
        return offset;
        
    }
    
    public void getSvg(PrintWriter writer, int maxwidth, int maxheight, int maxcell) {
        int minx = width;
        int miny = height;
        int maxx = 0;
        int maxy = 0;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (universe[lifespan][y][x] > 0) {
                    maxy = Math.max(y, maxy);
                    maxx = Math.max(x, maxx);
                    miny = Math.min(y, miny);
                    minx = Math.min(x, minx);
                }
            }
        }
        
        int[] offset = getOffset();
        int offx = offset[0];
        int offy = offset[1];
        
        String transparent = "0";
        String opaque = "1";
        if ((offx == 0) && (offy == 0)) {
            transparent = "0.3";
            
        }
        
        int effw = maxx - minx + 1 + Math.max(0, Math.abs(offx));
        int effh = maxy - miny + 1 + Math.max(0, Math.abs(offy));
        
        int gsize = maxcell;
        
        if (effw * gsize > maxwidth) {
            gsize = maxwidth/effw;
        }
        
        if (effh * gsize > maxheight) {
            gsize = maxheight/effh;
        }
        
        writer.println("<!-- SVG auto-generated by com.cp4space.catagolue.algorithms.SmallLife -->");
        
        writer.println("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"" +
        (-8*Math.max(0, offx)) + " " + (-8*Math.max(0, offy)) + " " + 8*effw + " " + 8*effh +
        "\" width=\"" + gsize*effw + "px\" height=\"" + gsize*effh + "px\">");

        long duration = Math.max(2, lifespan / 6);
        
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (universe[lifespan][y][x] > 0) {

                    String ot;
                    String ocolour;
                    if ((universe[0][y][x] & 1) == 1) {
                        ot = opaque; ocolour = "black";
                    } else if (universe[0][y][x] < 4) {
                        ot = transparent; ocolour = "black";
                    } else {
                        ot = String.valueOf(1.0 - (1.0 / (universe[0][y][x] / 2))); ocolour = "green";
                    }
                    
                    writer.println("<rect width=\"7\" height=\"7\" x=\"" + 8*(x - minx) + "\" y=\"" + 8*(y - miny) +
                            "\" rx=\"2\" ry=\"2\" fill=\"" + ocolour + "\" opacity=\"" + ot + "\">");
                    
                    // Opacity animation:
                    if (lifespan > 0) {
                        if ((lifespan > 1) || (offx != 0) || (offy != 0)) {
                            String animvals = (universe[0][y][x] == 1 ? opaque : transparent);
                            for (int i = 1; i < lifespan + 1; i++) {
                                animvals = animvals + ";" + (universe[i][y][x] == 1 ? opaque : transparent);
                                animvals = animvals + ";" + (universe[i][y][x] == 1 ? opaque : transparent);
                            }
                            writer.println("<animate attributeName=\"opacity\" values=\""+ animvals + "\" dur=\"" + duration + "s\" repeatCount=\"indefinite\"/>");
                        }
                    }
                    
                    // Abscissa animation:
                    if (offx != 0) {
                        String animvals = String.valueOf(8*(x - minx)) + ";" + String.valueOf(8*(x - minx) - (8*offx));
                        writer.println("<animate attributeName=\"x\" values=\""+ animvals + "\" dur=\"" + duration + "s\" repeatCount=\"indefinite\"/>");
                    }
                    
                    // Ordinate animation:
                    if (offy != 0) {
                        String animvals = String.valueOf(8*(y - miny)) + ";" + String.valueOf(8*(y - miny) - (8*offy));
                        writer.println("<animate attributeName=\"y\" values=\""+ animvals + "\" dur=\"" + duration + "s\" repeatCount=\"indefinite\"/>");
                    }
                    
                    writer.println("</rect>");
                }
            }
        }       
        
        writer.println("</svg>");
    }

}
