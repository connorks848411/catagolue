package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.parmenides.Parmenides;
import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.apphosting.api.ApiProxy;

public class VerifyServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(Census.class.getName());

    public void submitVerification(BufferedReader reader, PrintWriter writer, String displayedName) throws IOException {

        String rulestring = "b3s23";
        String symmetry = "C1";
        String md5 = "none";
        String passcode = "unknown";

        StringBuilder claimedData = new StringBuilder();

        String line = null;

        while ((line = reader.readLine()) != null) {
            if (line.length() >= 2) {
                if (line.charAt(0) == '@') {
                    // Header line:
                    String[] parts = line.substring(1).split(" ", 2);

                    try {
                        if (parts[0].equalsIgnoreCase("MD5") && parts.length >= 2) {
                            md5 = parts[1];
                        } else if (parts[0].equalsIgnoreCase("PASSCODE") && parts.length >= 2) {
                            passcode = parts[1];
                        } else if (parts[0].equalsIgnoreCase("RULE") && parts.length >= 2) {
                            rulestring = parts[1].toLowerCase();
                        } else if (parts[0].equalsIgnoreCase("SYMMETRY") && parts.length >= 2) {
                            symmetry = parts[1];
                        }
                    } catch (NumberFormatException e) {
                        // To stop saboteurs from crashing Catagolue by posting invalid data.
                    }
                } else {
                    // Ordinary line:
                    claimedData.append(line + "\n");
                }
            }
        }

        if (passcode.equals(Parmenides.remd5(md5))) {

            log.warning("Passcode is correct.");

            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

            Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", md5).getKey();

            try {
                Entity haul = datastore.get(haulKey);

                String privateData = ((Text) haul.getProperty("privateData")).getValue();
                Double chisquare = (Double) haul.getProperty("chisquare");
                Long status = (Long) haul.getProperty("committed");

                if ((status == null || status == Census.REJECTED || status == Census.UNVERIFIED)) { 

                    String errorMessage = null;

                    String[] lines = privateData.split("\n");

                    for (String pline : lines) {

                        if (pline.length() >= 3) {

                            String[] parts = pline.split(" ");

                            if (parts[2].equals("false")) {
                                errorMessage = "Invalid apgcode: "+parts[0];
                            }
                        }
                    }

                    Entity cachedCensus = Parmenides.retrieveTable(rulestring, symmetry);
                    Double tcs = (Double) cachedCensus.getProperty("meanchisquare");
                    double chisquare_threshold = (tcs + 3.0) * 1.7;

                    if (errorMessage != null) {

                    } else if (chisquare == null) {

                        errorMessage = "No chi-square test statistic exists.";

                    } else if (chisquare <= chisquare_threshold) {

                        Map<String, Long> firstMap = new HashMap<String, Long>();
                        Map<String, Long> secondMap = new HashMap<String, Long>();

                        log.warning("Comparing private data with claimed data: ");

                        Census.affineAddToMap(privateData, 1, firstMap, "");
                        Census.affineAddToMap(claimedData.toString(), 1, secondMap, "");

                        for (String apgcode : firstMap.keySet()) {
                            if (firstMap.get(apgcode) == null) {
                                errorMessage = (apgcode + " not found in first map.");
                            } else if (firstMap.get(apgcode) < 0) {
                                errorMessage = (apgcode + " occurred a negative number of times.");
                            } else if (secondMap.get(apgcode) == null) {
                                errorMessage = (apgcode + " not found in second map.");
                            } else if (secondMap.get(apgcode) < Math.min(10, firstMap.get(apgcode))) {
                                errorMessage = ("Insufficiently many occurrences of " + apgcode + " in second map.");
                            } else if (apgcode.equals("PATHOLOGICAL")) {
                                errorMessage = ("Hauls containing PATHOLOGICAL are automatically rejected.");
                            } else {
                                // log.warning("Correct number of occurrences (namely "+String.valueOf(secondMap.get(apgcode))+") of " + apgcode);
                            }
                        }

                        log.warning("Comparison is complete.");
                    } else {
                        errorMessage = "Haul fails chi-square test: "+String.valueOf(chisquare)+" > "+String.valueOf(chisquare_threshold);
                    }

                    if (errorMessage == null) {
                        log.warning("Haul "+md5+" passed verification.");
                        haul.setProperty("committed", Census.VERIFIED);
                    } else {
                        log.warning("Haul "+md5+" failed verification.");
                        log.severe(errorMessage);
                        haul.setProperty("committed", Census.REJECTED);
                    }

                    haul.setUnindexedProperty("verifierName", displayedName);

                    datastore.put(haul);
                }

            } catch (EntityNotFoundException e) {

            }

        } else {
            log.warning("Passcode is incorrect.");
        }

    }


    public void getHaulCertificate(BufferedReader reader, PrintWriter writer) throws IOException {

        String rulestring = reader.readLine();
        String symmetry = reader.readLine();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        log.warning("Rulestring: "+rulestring+", symmetry: "+symmetry);

        Filter propertyFilter = new FilterPredicate("committed",
                FilterOperator.EQUAL,
                Census.UNVERIFIED);

        Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);

        int limit = 30;

        List<Entity> hauls = preparedQuery.asList(FetchOptions.Builder.withLimit(limit));

        if (hauls.size() > 0) {
            // We randomly decide which haul to process:
            int randomNumber = Long.valueOf(ApiProxy.getCurrentEnvironment().getRemainingMillis()).intValue();
            Entity haul = hauls.get(randomNumber % hauls.size());

            String publicData = ((Text) haul.getProperty("publicData")).getValue();

            String md5 = haul.getKey().getName();
            String md5b = Parmenides.remd5(md5);

            writer.println("yes");
            writer.println(md5);
            writer.println(md5b);
            writer.print(publicData);
        } else {
            writer.println("no");
        }

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();

        String payoshaRequest = reader.readLine();

        String payoshaResponse = PayoshaUtils.callPayosha("catagolue", payoshaRequest);
        String[] parts = payoshaResponse.split(":", 4);

        if ((parts[0].equals("payosha256")) && (parts[1].equals("good"))) {
            writer.println("Payosha256 authentication succeeded.");

            if (parts[2].equals("verify_apgsearch_haul")) {
                getHaulCertificate(reader, writer);
            } else if (parts[2].equals("submit_verification")) {
                submitVerification(reader, writer, parts[3]);
            }
        } else {
            writer.println("Payosha256 authentication failed with the following response:");
            writer.println(payoshaResponse);
        }

        writer.println("***********************************************");
    }

}
