package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EchoServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        
        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();
        
        String line = null;
        int i = 0;
        
        writer.println("*** Catagolue echo service received the following data: ***");
        
        while ((line = reader.readLine()) != null) {
            i++;
            writer.println(i + ": " + line);
        }
        
        writer.println("***********************************************************");
    }
}
