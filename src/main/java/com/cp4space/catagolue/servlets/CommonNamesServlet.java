package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.payosha256.PayoshaUtils;
import com.cp4space.catagolue.census.CommonNames;

public class CommonNamesServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        
        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();
        
        String line = reader.readLine();
        String password = line.split(" ")[0];

        if (!PayoshaUtils.validatePassword(password)) {
            writer.println("Invalid password.");
            return;
        } else {
            writer.println("Valid password.");
        }

        String command = line.split(" ")[1];
        String namespace = line.split(" ")[2];

        Map<String, String> namemap = new HashMap<String, String>();

        if (command.equals("UPDATE")) {
            CommonNames.getNamemap(namespace, namemap, false);
        } else if (command.equals("OVERWRITE")) {
            // do nothing;
        } else {
            writer.println("Unrecognised operation");
            return;
        }
        
        while ((line = reader.readLine()) != null) {
            if (line.length() >= 2) {
                String[] parts = line.split(" ", 2);
                namemap.put(parts[0], parts[1]);
            }
        }

        CommonNames.putNamemap(namespace, namemap);

        writer.println("Operation successful.");
    }
}
