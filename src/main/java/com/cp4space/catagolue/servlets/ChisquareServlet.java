package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.servlets.CensusServlet;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.parmenides.Parmenides;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class ChisquareServlet extends HttpServlet {

    public static Census getHaulCensus(Entity haul, String rulestring, String symmetry, Entity cachedCensus, PrintWriter writer) {

        Census haulCensus = new Census();
        String root = (String) haul.getProperty("root");
        long claimedobjs = (long) haul.getProperty("numobjects");
        haulCensus.affineAdd(((Text) haul.getProperty("censusData")).getValue(), 1);
        haulCensus.includeSamples(((Text) haul.getProperty("soupData")).getValue(), symmetry, root, null);
        double chisquaretotal = Parmenides.chisquare(cachedCensus, haulCensus, claimedobjs);
        if (writer != null) { writer.println(root+": "+String.valueOf(chisquaretotal)); }
        return haulCensus;

    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();

        if (!CensusServlet.isAdmin()) {
            writer.println("Statistical tests require superuser privileges.");
            return;
        }

        String haulcode = req.getParameter("haulcode");
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4) && (haulcode == null)) {
                haulcode = pathParts[3];
            }
        }

        // Obtain the overall census data:

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        Entity cachedCensus = Parmenides.retrieveTable(rulestring, symmetry);

        if (haulcode == null) {

            Filter propertyFilter = new FilterPredicate("committed", FilterOperator.EQUAL, Census.JUST_COMMITTED);
            Query query2 = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
            PreparedQuery preparedQuery2 = datastore.prepare(query2);
            int maxhauls = 50;
            List<Entity> hauls = preparedQuery2.asList(FetchOptions.Builder.withLimit(maxhauls));

            for (Entity haul : hauls) {
                getHaulCensus(haul, rulestring, symmetry, cachedCensus, writer);
            }

        } else {
            try {
                Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", haulcode).getKey();
                Entity haul = datastore.get(haulKey);
                Census haulCensus = getHaulCensus(haul, rulestring, symmetry, cachedCensus, writer);

                StringBuilder privateData = new StringBuilder();
                StringBuilder publicData = new StringBuilder();

                Parmenides.rareObjects(rulestring, cachedCensus, haulCensus, privateData, publicData);

                writer.println("Private data:\n\n"+privateData.toString());
                writer.println("Public data:\n\n"+publicData.toString());

            } catch (EntityNotFoundException e) {
                writer.println("The specified haul appears not to exist.");
            }
        }
    }
}
