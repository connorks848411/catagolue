package com.cp4space.catagolue.census;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.Set;
import java.util.TreeSet;

import com.cp4space.catagolue.parmenides.Parmenides;
import com.cp4space.catagolue.servlets.CensusServlet;
import com.cp4space.catagolue.servlets.ObjectServlet;
import com.cp4space.catagolue.servlets.AttributeServlet;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.apphosting.api.ApiProxy;

public class Census {

    private static final Logger log = Logger.getLogger(Census.class.getName());

    public static int JUST_UPLOADED = 0;
    public static int JUST_COMMITTED = 2;

    public static int UNVERIFIED = 1;
    public static int VERIFIED = 3;
    public static int REJECTED = 4;

    public Map<String, Long> table;
    public Map<String, Set<String>> samples;

    public Census() {
        table = new HashMap<String, Long>();
        samples = new HashMap<String, Set<String>>();
    }

    public long totobjs() {
        long to = 0;
        for (Map.Entry<String, Long> entry : table.entrySet()) {
            long quantity = entry.getValue();
            to += quantity;
        }
        return to;
    }

    public static String apgprefix(String apgcode) {

        if (apgcode == null) { return "_"; }
        String prefix = apgcode.split("_")[0];
        if (prefix.length() == 0) { return "_"; }
        return prefix;

    }

    public static String serialiseSortTable(Map<String, Long> map, int limit, boolean swap) {
        String newBoard = "";

        int i = 0;

        for (Map.Entry<String, Long> entry : sortTable(map)) {

            if (i == limit) {
                break;
            }

            String displayedName = entry.getKey();
            long quantity = entry.getValue();

            if (swap) {
                newBoard += displayedName + " " + String.valueOf(quantity) + "\n";
            } else {
                newBoard += String.valueOf(quantity) + " " + displayedName + "\n";
            }

            i++;
        }

        return newBoard;
    }

    public static String serialiseSortTable(Map<String, Long> map) {
        return serialiseSortTable(map, -1, false);
    }

    public static TreeSet<Map.Entry<String, Long>> sortTable(Map<String, Long> map) {

        // Define a custom comparator to sort frequencies in descending order
        // (with objects of the same frequency in lexicographical order):
        Comparator<Map.Entry<String, Long>> comparator = new Comparator<Map.Entry<String, Long>>(){
            @Override
                public int compare(final Map.Entry<String, Long> o1, final Map.Entry<String, Long> o2){
                    int x = -o1.getValue().compareTo(o2.getValue());
                    if (x == 0) {
                        return o1.getKey().compareTo(o2.getKey());
                    } else {
                        return x;
                    }
                }
        };

        TreeSet<Map.Entry<String, Long>> treeSet = new TreeSet<Entry<String, Long>>(comparator);
        treeSet.addAll(map.entrySet());

        return treeSet;

    }

    public TreeSet<Map.Entry<String, Long>> sortedTable() {

        return sortTable(table);
    }

    public static void appendMetadatumLowmem(DatastoreService datastore, String rulestring, String datumName, Map<String, String> mdatumlist) {

        Map<String, String> msublist = new HashMap<String, String>();

        int i = 0;
        int chunkSize = 5000;

        for (Entry<String, String> ms : mdatumlist.entrySet()) {

            msublist.put(ms.getKey(), ms.getValue()); i += 1;

            if (i % chunkSize == 0) {

                log.warning("-- processing objects "+String.valueOf(i - chunkSize + 1)+" to "+String.valueOf(i));
                appendMetadatumParallel(datastore, rulestring, datumName, msublist);
                msublist.clear();
                try {
                    log.warning("-- sleeping for 1000 milliseconds");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.severe("InterruptedException encountered whilst sleeping.");
                }
            }

        }

        if (i == 0) { return; }

        log.warning("-- processing objects "+String.valueOf(i - (i % chunkSize) + 1)+" to "+String.valueOf(i));
        appendMetadatumParallel(datastore, rulestring, datumName, msublist);

        try {
            log.warning("-- sleeping for 1000 milliseconds");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.severe("InterruptedException encountered whilst sleeping.");
        }

    }

    public static void appendMetadatumParallel(DatastoreService datastore, String rulestring, String datumName, Map<String, String> mdatumlist) {

        Set<Key> setOfKeys = new TreeSet<Key>();

        for (String apgcode : mdatumlist.keySet()) {
            Key datumKey = new KeyFactory.Builder("Rule", rulestring).addChild("Object", apgcode).addChild("Metadatum", datumName).getKey();
            setOfKeys.add(datumKey);
        }

        Map<Key, Entity> metadataEntities = datastore.get(setOfKeys);

        for (String apgcode : mdatumlist.keySet()) {
            Key datumKey = new KeyFactory.Builder("Rule", rulestring).addChild("Object", apgcode).addChild("Metadatum", datumName).getKey();
            if (metadataEntities.get(datumKey) == null) {
                Entity metadataEntity = new Entity(datumKey);
                metadataEntity.setProperty("data", new Text(mdatumlist.get(apgcode)));
                metadataEntities.put(datumKey, metadataEntity);
            } else {
                Entity metadataEntity = metadataEntities.get(datumKey);
                String oldData = ((Text) metadataEntity.getProperty("data")).getValue();
                metadataEntity.setProperty("data", new Text(oldData + mdatumlist.get(apgcode)));
            }
        }

        datastore.put(metadataEntities.values());
    }

    public static void appendMetadatum(DatastoreService datastore, String rulestring, String apgcode, String datumName, String metadatum) {
        Key datumKey = new KeyFactory.Builder("Rule", rulestring).addChild("Object", apgcode).addChild("Metadatum", datumName).getKey();

        try {
            Entity metadataEntity = datastore.get(datumKey);
            String oldData = ((Text) metadataEntity.getProperty("data")).getValue();
            metadataEntity.setProperty("data", new Text(oldData + metadatum));
            datastore.put(metadataEntity);
        } catch (EntityNotFoundException e) {
            Entity metadataEntity = new Entity(datumKey);
            metadataEntity.setProperty("data", new Text(metadatum));
            datastore.put(metadataEntity);
        }
    }

    public static String getMetadatum(DatastoreService datastore, String rulestring, String apgcode, String datumName) {
        Key datumKey = new KeyFactory.Builder("Rule", rulestring).addChild("Object", apgcode).addChild("Metadatum", datumName).getKey();

        try {
            Entity metadataEntity = datastore.get(datumKey);
            String oldData = ((Text) metadataEntity.getProperty("data")).getValue();
            return oldData;
        } catch (EntityNotFoundException e) {
            return null;
        }       
    }

    public static void logLine(PrintWriter writer, String message) {

        writer.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z").format(new Date()) + " -- " + message);
        log.info(message);

    }

    public static void updateLeaderboard(DatastoreService datastore, List<Entity> hauls, Key boardKey, String property) {

        Entity leaderboard;
        Map<String, Long> scoremap = new HashMap<String, Long>();

        try {
            leaderboard = datastore.get(boardKey);

            String[] lines = ((Text) leaderboard.getProperty("data")).getValue().split("\n");

            for (String line : lines) {

                if (line != null && line.length() >= 1) {

                    String[] parts = line.split(" ");

                    String displayedName;
                    long quantity;

                    if (parts[0].matches("[0-9]*")) {
                        displayedName = parts[1];
                        for (int i = 2; i < parts.length; i++) {
                            displayedName += " " + parts[i];
                        }
                        quantity = Long.valueOf(parts[0]);
                    } else {
                        displayedName = parts[0];
                        for (int i = 1; i < parts.length - 1; i++) {
                            displayedName += " " + parts[i];
                        }
                        quantity = Long.valueOf(parts[parts.length - 1]);                       
                    }

                    if (scoremap.containsKey(displayedName)) {
                        scoremap.put(displayedName, scoremap.get(displayedName) + quantity);
                    } else {
                        scoremap.put(displayedName, quantity);
                    }
                }
            }

        } catch (EntityNotFoundException e) {
            leaderboard = new Entity(boardKey);
        }

        for (Entity haul : hauls) {
            String displayedName = (String) haul.getProperty("displayedName");
            long quantity = (long) haul.getProperty(property);

            if (scoremap.containsKey(displayedName)) {
                scoremap.put(displayedName, scoremap.get(displayedName) + quantity);
            } else {
                scoremap.put(displayedName, quantity);
            }
        }

        leaderboard.setProperty("data", new Text(serialiseSortTable(scoremap)));

        datastore.put(leaderboard);

    }

    public void backoutHaul(PrintWriter writer, String rulestring, String symmetry, String haulcode, long aq) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", haulcode).getKey();
        try {
            Entity haul = datastore.get(haulKey);
            affineAdd(((Text) haul.getProperty("censusData")).getValue(), -aq);
            updateTabulations(rulestring, symmetry);
            writer.println("Successful.");
        } catch (EntityNotFoundException e) {

        }

    }

    public static boolean isCensusLarge(String rulestring, String symmetry) {

        if (ObjectServlet.isUnofficial(symmetry) >= 2) {
            log.warning("Symmetry is unofficial; omitting Parmenides...");
            return false;
        } else if (!rulestring.matches("b[1-8ceaiknjqrytwz-]*s[0-8ceaiknjqrytwz-]*")) {
            log.warning("Not an isotropic 2-state non-B0 Moore rule; omitting Parmenides...");
            return false;
        }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        try {
            Entity census = datastore.get(censusKey);
            long censusObjects = (long) census.getProperty("numobjects");
            long oneMillion = 1000000;
            long oneTrillion = oneMillion * oneMillion;
            if (censusObjects >= oneTrillion) {
                log.warning("Census contains > 10^12 objects; using Parmenides...");
                return true;
            } else {
                log.warning("Census contains < 10^12 objects; omitting Parmenides...");
                return false;
            }
        } catch (EntityNotFoundException e) {
            log.warning("Census is completely new; omitting Parmenides...");
            return false;
        }

    }

    public static void fullCronJob(PrintWriter writer, String rulestring, String symmetry) {

        boolean useParmenides = isCensusLarge(rulestring, symmetry);
        int limit = (rulestring.length() >= 10) ? 20 : 60;
        if (rulestring.equals("b3s23")) { limit = 120; }
        if (rulestring.equals("b45s12")) { limit = 1; }

        Census census = new Census();
        Map<String, String> mdatumlist = census.processHauls(writer, rulestring, symmetry, useParmenides, limit);

        if (mdatumlist == null) { return; }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        if (mdatumlist.size() > 0) {
            logLine(writer, "Writing sample soups for "+String.valueOf(mdatumlist.size())+" objects.");
            appendMetadatumLowmem(datastore, rulestring, "samples", mdatumlist);
            logLine(writer, "Sample soups saved in Catagolue metadata.");
        }

        if (useParmenides) {
            runParmenides(writer, rulestring, symmetry, limit);
        }

        if (rulestring.equals("b3s23") && symmetry.equals("C1")) {

            logLine(writer, "Attributing objects...");

            try {
                log.warning("-- sleeping for 1000 milliseconds");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.severe("InterruptedException encountered whilst sleeping.");
            }

            for (String apgcode : mdatumlist.keySet()) {

                if ((apgcode.charAt(1) != 's') || (apgcode.charAt(2) >= '3')) {
                    log.warning(" -- attributing object " + apgcode);
                    AttributeServlet.attributeObject(apgcode, rulestring, symmetry, datastore, writer);
                } else {
                    writer.println(apgcode + " is deemed insufficiently interesting.");
                }

            }

            logLine(writer, "...objects attributed.");
        }

        deleteHauls(writer, rulestring, symmetry);

    }

    public Map<String, String> processHauls(PrintWriter writer, String rulestring, String symmetry, boolean useParmenides, int limit) {

        log.warning("Processing hauls for "+rulestring+"/"+symmetry+"...");

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        Filter propertyFilter = new FilterPredicate("committed",
                FilterOperator.EQUAL,
                (useParmenides ? VERIFIED : JUST_UPLOADED));
        Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);

        List<Entity> hauls = preparedQuery.asList(FetchOptions.Builder.withLimit(limit));

        if (limit == 0) {
            logLine(writer, "No hauls allowed.");
            return null;
        }

        logLine(writer, hauls.size() + " hauls found.");
        log.warning(hauls.size() + " hauls found.");

        long newobjects = 0;
        long newsoups = 0;

        // Load the hauls into the census:
        for (Entity haul : hauls) {
            affineAdd(((Text) haul.getProperty("censusData")).getValue(), 1);
            newobjects += (long) haul.getProperty("numobjects");
            newsoups += (long) haul.getProperty("numsoups");
            // Mark the haul as committed:
            haul.setProperty("committed", JUST_COMMITTED);
        }

        logLine(writer, "Hauls loaded into census.");

        // Update the committed status of the hauls in the datastore:
        datastore.put(hauls);
        logLine(writer, "Hauls marked as committed.");

        // Add the census to the existing tabulations:
        Map<String, Boolean> isTooLarge = updateTabulations(rulestring, symmetry);

        logLine(writer, "Tabulations updated.");

        // Update (or, if necessary, create) the parent census:
        // Do this really quickly to make it practically atomic:
        try {
            Entity census = datastore.get(censusKey);
            census.setProperty("uncommittedHauls", 0);
            census.setProperty("committedHauls", ((long) census.getProperty("committedHauls")) + hauls.size());
            census.setProperty("lastModified", new Date());
            census.setProperty("numsoups", ((long) census.getProperty("numsoups")) + newsoups);
            census.setProperty("numobjects", ((long) census.getProperty("numobjects")) + newobjects);
            datastore.put(census);
        } catch (EntityNotFoundException e) {
            Entity census = new Entity("Census", rulestring + "/" + symmetry);
            census.setProperty("uncommittedHauls", 0);
            census.setProperty("committedHauls", hauls.size());
            census.setProperty("symmetries", symmetry);
            census.setProperty("rulestring", rulestring);
            census.setProperty("numsoups", newsoups);
            census.setProperty("numobjects", newobjects);
            census.setProperty("lastModified", new Date());
            datastore.put(census);
        }

        logLine(writer, "Census updated.");

        // Update leaderboard:
        updateLeaderboard(datastore, hauls, new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Leaderboard", "numobjects").getKey(), "numobjects");

        logLine(writer, "Leaderboard updated.");

        Map<String, String> mdatumlist = new HashMap<String, String>();

        if (symmetry.length() > 50) {

            logLine(writer, "Skipping sample soups owing to long symmetry name.");

        } else {

            // Load sample soups:
            for (Entity haul : hauls) {
                includeSamples(
                        ((Text) haul.getProperty("soupData")).getValue(),
                        symmetry, (String) haul.getProperty("root"),
                        (rulestring.equals("b3s23") ? null : isTooLarge));
            }

            logLine(writer, "Sample soups loaded.");

            // Save the sample soups in the Catagolue:

            int inclimit = (symmetry.equals("C1") ? 500 : 200);
            if (ObjectServlet.isUnofficial(symmetry) >= 2) { inclimit = 100; }

            for (String apgcode : samples.keySet()) {
                if (apgcode != null && table.containsKey(apgcode) && (table.get(apgcode) <= inclimit)) {

                    String prefix = apgprefix(apgcode);

                    // Don't include sample soups for massive tabulations:
                    if (rulestring.equals("b3s23") || isTooLarge.get(prefix) == null || isTooLarge.get(prefix) == false) {

                        StringBuilder mdatumbuilder = new StringBuilder();

                        for (String line : samples.get(apgcode)) {
                            mdatumbuilder.append(line + "\n");
                        }

                        mdatumlist.put(apgcode, mdatumbuilder.toString());
                    }
                }
            }
        }

        // Liberate memory:
        samples.clear();
        table.clear();

        return mdatumlist;

    }

    public static void deleteHauls(PrintWriter writer, String rulestring, String symmetry) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        int hoffset = (rulestring.equals("b3s23") ? (symmetry.equals("C1") ? 10000 : 5000) : 100);

        Filter propertyFilter = new FilterPredicate("committed", FilterOperator.EQUAL, 2);
        Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query.setKeysOnly());
        List<Entity> hauls = preparedQuery.asList(FetchOptions.Builder.withDefaults().offset(hoffset).limit(200));

        if (hauls.size() > 0) {
            logLine(writer, "Deleting " + hauls.size() + " hauls...");
            List<Key> itemsToDelete = new ArrayList<Key>();
            for (Entity haul : hauls) { itemsToDelete.add(haul.getKey()); }
            datastore.delete(itemsToDelete);
        }

    }

    public static void runParmenides(PrintWriter writer, String rulestring, String symmetry, int limit) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        logLine(writer, "Running Parmenides...");

        Filter propertyFilter = new FilterPredicate("committed", FilterOperator.EQUAL, JUST_UPLOADED);
        Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);

        List<Entity> hauls = preparedQuery.asList(FetchOptions.Builder.withLimit(limit));

        logLine(writer, hauls.size() + " hauls found.");

        if (hauls.size() > 0) {

            Entity cachedCensus = Parmenides.retrieveTable(rulestring, symmetry);

            for (Entity haul : hauls) {

                Census haulCensus = new Census();
                String root = (String) haul.getProperty("root");
                haulCensus.affineAdd(((Text) haul.getProperty("censusData")).getValue(), 1);
                haulCensus.includeSamples(((Text) haul.getProperty("soupData")).getValue(), symmetry, root, null);
                long claimedobjs = (long) haul.getProperty("numobjects");
                double chisquaretotal = Parmenides.chisquare(cachedCensus, haulCensus, claimedobjs);
                writer.println(root+": "+String.valueOf(chisquaretotal));

                haul.setProperty("committed", UNVERIFIED);

                StringBuilder privateData = new StringBuilder();
                StringBuilder publicData = new StringBuilder();

                Parmenides.rareObjects(rulestring, cachedCensus, haulCensus, privateData, publicData);

                haul.setUnindexedProperty("chisquare", chisquaretotal);
                haul.setProperty("privateData", new Text(privateData.toString()));
                haul.setProperty("publicData", new Text(publicData.toString()));

            }

            datastore.put(hauls);
        }

        logLine(writer, "...Parmenides completed!");
    }

    /**
     * Load tabulations from the Datastore and update them.
     * @param rulestring  e.g. b3s23
     * @param symmetry  e.g. C1
     */
    public Map<String, Boolean> updateTabulations(String rulestring, String symmetry) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        // Obtain set of prefices (each one corresponding to a tabulation):
        TreeSet<String> prefices = new TreeSet<String>();
        for (String apgcode : table.keySet()) {
            prefices.add(apgprefix(apgcode));
        }

        // Create a map of tabulations:
        Map<String, Map<String, Long>> prefixmap = new HashMap<String, Map<String, Long>>();
        for (String prefix : prefices) {
            prefixmap.put(prefix, new HashMap<String, Long>());
        }

        // Split census amongst tabulations:
        for (Map.Entry<String, Long> entry : table.entrySet()) {
            String apgcode = entry.getKey();
            long quantity = entry.getValue();
            if (quantity != 0) {
                String prefix = apgprefix(apgcode);
                prefixmap.get(prefix).put(apgcode, quantity);
            }
        }

        Map<String, Boolean> isTooLarge = new HashMap<String, Boolean>();       

        // Modify relevant tabulations:
        for (String prefix : prefices) {

            Map<String, Long> tcomb = prefixmap.get(prefix);
            Key tabulationKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Tabulation", prefix).getKey();
            try {
                Entity tabulation = datastore.get(tabulationKey);
                String censusData = Tabulation.getCensusData(tabulation);
                affineAddToMap(censusData, 1, tcomb, "");
            } catch (EntityNotFoundException e) {
            }

            Tabulation tnew = new Tabulation(prefix);

            boolean nonempty = false;

            for (Map.Entry<String, Long> entry : sortTable(tcomb)) {
                String apgcode = entry.getKey();
                long quantity = entry.getValue();

                if (table.containsKey(apgcode)) { table.put(apgcode, quantity); }

                int compresslimit = 300000;

                if (quantity != 0) {
                    nonempty = true;
                    tnew.appendRow(apgcode, quantity, compresslimit);
                }
            }

            if (nonempty) {
                Entity tab = new Entity("Tabulation", prefix, censusKey);
                boolean tooLarge = tnew.writeTab(tab);
                isTooLarge.put(prefix, tooLarge);
                datastore.put(tab);
            } else {
                datastore.delete(tabulationKey);
            }

            tcomb.clear();
        }

        return isTooLarge;

    }

    public void includeSamples(String soupData, String symmetry, String root, Map<String, Boolean> isTooLarge) {

        String[] lines = soupData.split("\n");

        for (String line : lines) {

            String[] parts = line.split(" ");
            String apgcode = parts[0];
            String prefix = apgprefix(apgcode);

            // Don't include sample soups for massive tabulations:
            if (isTooLarge == null || isTooLarge.get(prefix) == null || isTooLarge.get(prefix) == false) {

                if (samples.containsKey(apgcode) == false) {
                    samples.put(apgcode, new TreeSet<String>());
                }

                Set<String> stringSet = samples.get(apgcode);
                String longroot = symmetry + "/" + root;

                for (int i = 1; i < parts.length; i++) { stringSet.add(longroot + parts[i]); }
            }
        }
    }

    public static void affineAddToMap(String censusData, long multiple, Map<String, Long> map, String prefix, boolean disj) {

        String[] lines = censusData.split("\n");

        for (String line : lines) {

            if (line.length() >= 2) {

                String[] parts = line.split(" ");

                try {
                    if (parts.length >= 2) {

                        String apgcode = prefix + parts[0];
                        long quantity = Long.valueOf(parts[1]);

                        if (map.containsKey(apgcode)) {
                            map.put(apgcode, map.get(apgcode) + multiple * quantity);
                        } else if (disj) {
                            map.put(apgcode, multiple * quantity);
                        }
                    }
                } catch (NumberFormatException e) {
                    // To stop saboteurs from crashing Catagolue by posting invalid data.
                }
            }           
        }       
    }

    public static void affineAddToMap(String censusData, long multiple, Map<String, Long> map, String prefix) {
        affineAddToMap(censusData, multiple, map, prefix, true);
    }

    public void affineAdd(String censusData, long multiple, boolean disj) {
        affineAddToMap(censusData, multiple, table, "", disj);
    }

    public void removeKeys(Map<String, Long> map) {
        for (Map.Entry<String, Long> entry : map.entrySet()) {
            table.remove(entry.getKey());
        }
    }

    public void affineAdd(String censusData, long multiple) {
        affineAdd(censusData, multiple, true);
    }
}
