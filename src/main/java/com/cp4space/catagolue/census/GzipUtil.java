package com.cp4space.catagolue.census;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
 
public class GzipUtil {
 
    public static byte[] zip(final String str, final String method) {
        if ((str == null) || (str.length() == 0)) {
            throw new IllegalArgumentException("Cannot zip null or empty string");
        }
 
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            if (method == "gz") {
                try (GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream)) {
                    gzipOutputStream.write(str.getBytes(StandardCharsets.UTF_8));
                }
            } else if (method == "bz") {
                try (BZip2CompressorOutputStream bzipOutputStream = new BZip2CompressorOutputStream(byteArrayOutputStream)) {
                    bzipOutputStream.write(str.getBytes(StandardCharsets.UTF_8));
                }
            } else {
                throw new IllegalArgumentException("Method must be 'gz' or 'bz'");
            }
            return byteArrayOutputStream.toByteArray();
        } catch(IOException e) {
            throw new RuntimeException("Failed to zip content", e);
        }
    }
 
    public static BufferedReader uncompressStream(final byte[] compressed) {
        if ((compressed == null) || (compressed.length == 0)) {
            throw new IllegalArgumentException("Cannot unzip null or empty bytes");
        }
 
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(compressed);
            InputStreamReader inputStreamReader;

            if (isZipped(compressed)) {
                GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
                inputStreamReader = new InputStreamReader(gzipInputStream, StandardCharsets.UTF_8);
            } else {
                BZip2CompressorInputStream bzipInputStream = new BZip2CompressorInputStream(byteArrayInputStream);
                inputStreamReader = new InputStreamReader(bzipInputStream, StandardCharsets.UTF_8);
            }

            return new BufferedReader(inputStreamReader);
        } catch (IOException e) {
            throw new RuntimeException("Failed to unzip content", e);
        }
    }

    public static String unzip(final byte[] compressed) {
        try {
            BufferedReader bufferedReader = uncompressStream(compressed);
            StringBuilder output = new StringBuilder();
            String line = null;
            boolean firstLine = true;
            while ((line = bufferedReader.readLine()) != null) {
                if (!firstLine) { output.append("\n"); }
                output.append(line);
                firstLine = false;
            }
            return output.toString();
        } catch (IOException e) {
            throw new RuntimeException("Failed to unzip content", e);
        }

    }
 
    public static boolean isZipped(final byte[] compressed) {
        return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
    }
}
