package com.cp4space.catagolue.census;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;
import java.io.BufferedReader;
import java.io.StringReader;

public class Tabulation {

    String prefix;
    public StringBuilder dataBuilder;
    int rows;
    double compression;
    int compressionMode;

    public Tabulation(String apgPrefix) {
        prefix = apgPrefix;
        dataBuilder = new StringBuilder();
        rows = 0;
        compression = 1.0;
        compressionMode = 0;
    }

    public double getCompressionRatio() { return compression; }

    static int MAX_BYTES = 980000;

    /**
     * Appends a row to the tabulation.
     * @param apgcode  the representation of the object, such as "xp15_4r4z4r4"
     * @param quantity  the number of occurrences of the object
     */
    public void appendRow(String apgcode, long quantity, int compressThreshold) {

        if ((compressionMode == 0) && (dataBuilder.length() >= compressThreshold)) {
            /*
            * Now that we exceed the compression threshold, we use gzip to
            * reduce the tabulation size.
            */

            String datastring = dataBuilder.toString();
            byte[] comp = GzipUtil.zip(datastring, "gz");

            compression = ((double) datastring.length()) / ((double) comp.length);
            compressionMode = 1;

        }

        if ((compressionMode == 1) && (dataBuilder.length() >= (900000 * compression))) {
            /*
            * The estimated compressed size is 900 kB. We now compare gzip and
            * bzip2 to see which delivers the higher compression ratio, and
            * then use that. (Almost certainly bzip2 will win.)
            */

            String datastring = dataBuilder.toString();
            byte[] bzcomp = GzipUtil.zip(datastring, "bz");
            byte[] gzcomp = GzipUtil.zip(datastring, "gz");

            double gz_compression = ((double) datastring.length()) / ((double) gzcomp.length);
            double bz_compression = ((double) datastring.length()) / ((double) bzcomp.length);
            if (bz_compression > gz_compression) {
                compression = bz_compression;
                compressionMode = 2;
            } else {
                compression = gz_compression;
                compressionMode = 3;
            }

        }

        // Don't exceed the hardcoded Google App Engine limitations:
        if (dataBuilder.length() < (MAX_BYTES * compression)) {
            if (rows != 0) {
                dataBuilder.append("\n");
            }
            dataBuilder.append(apgcode + " " + String.valueOf(quantity));
            rows += 1;
        }
    }

    public boolean writeTab(Entity tab) {

        String datastring = dataBuilder.toString();

        if (compressionMode == 0) {
            tab.setProperty("compressed", false);
            tab.setProperty("censusData", new Text(datastring));
        } else {
            String method = (compressionMode == 2) ? "bz" : "gz";
            byte[] comp = GzipUtil.zip(datastring, method);
            tab.setProperty("compressed", true);
            tab.setProperty("censusData", new Blob(comp));
        }

        return (datastring.length() >= (MAX_BYTES * compression));
    }

    public static String getCensusData(Entity tab) {

        Boolean gzipped = (Boolean) tab.getProperty("compressed");

        if ((gzipped == null) || (gzipped == false)) {
            return ((Text) tab.getProperty("censusData")).getValue();
        } else {
            byte[] comp = ((Blob) tab.getProperty("censusData")).getBytes();
            return GzipUtil.unzip(comp);
        }
    }

    public static BufferedReader getCensusStream(Entity tab) {

        Boolean gzipped = (Boolean) tab.getProperty("compressed");

        if ((gzipped == null) || (gzipped == false)) {
            String tabulationData = ((Text) tab.getProperty("censusData")).getValue();
            return new BufferedReader(new StringReader(tabulationData));
        } else {
            byte[] comp = ((Blob) tab.getProperty("censusData")).getBytes();
            return GzipUtil.uncompressStream(comp);
        }
    }
}
